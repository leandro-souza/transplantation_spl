#!/bin/bash
echo "Downloading txl package..."
wget https://www.txl.ca/ndownload/9325-txl10.6e.macosx64.tar.gz

echo "Extracting txl package..."
tar xvzf 9325-txl10.6e.macosx64.tar.gz

cd txl10.6e.macosx64

echo "Installing txl..."
sudo ./InstallTxl

cd ..
echo "Removing downloaded txl files..."
rm -r -f 9325-txl10.6e.macosx64.tar.gz
rm -r -f txl10.6e.macosx64
