#!/bin/bash

ulimit -s unlimited

cd ..

TXL/readReverseCallGraph.x Mytar/input_files/temp_output_reversed_CFLOW.out Mytar/Donor/ Mytar/output_files/outputFCTLOCS.out Mytar/output_files/SkeletonInstantiateMappings.out Mytar/output_files/HostSymbolTable.out Mytar/output_files/CurrentExistingMappings.out TXL/ifdef.x Mytar/input_files/gcovParsedResultsCalledFunctions.out Mytar/output_files/temp_output_labels.out  > /dev/null 2>Mytar/errorFile.out
