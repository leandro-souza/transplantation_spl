#!/bin/bash

#ulimit -s unlimited
ulimit -s 65353
cd ..

TXL/readReverseCallGraph.x MacVIM/input_files/temp_output_reversed_CFLOW.out MacVIM/macvim/src/ MacVIM/output_files/outputFCTLOCS.out MacVIM/output_files/SkeletonInstantiateMappings.out MacVIM/output_files/HostSymbolTable.out MacVIM/output_files/CurrentExistingMappings.out TXL/ifdef.x MacVIM/input_files/gcovParsedResultsCalledFunctions.out MacVIM/output_files/temp_output_labels.out  > /dev/null 2>MacVIM/errorFile.out
