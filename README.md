# README: transplantation_spl
MAINTAINER Leandro Souza "leouneb@gmail.com"


For allowing the execution of our TXL program (readReverseCallGraph.x) in different environments, we have provided two ways for running it:

1) Install txl (If TXL has not been installed).

   - Mac OS X 10.10 or later (64 bit) Xcode 7.0 or later command line tools
    - REQUIRE homebrew and wget:
        -   Case Homebrew is not already installed
        
                    RUN: ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

        -  Case wget is not already installed
        
                    RUN: brew install wget
        
    -  To Install txl RUN:

                RUN: ./txl_config_mac_os.sh
   
   
- Linux x86 (32 bit) for Ubuntu, Debian, Fedora, Red Hat, Mageia
        gcc 4.7 or later
        
            RUN: ./txl_config_Linux x86.sh

- Check if txl have been installed:

        RUN: txl --version
        Result: TXL v10.6e (9.5.17) (c) 1988-2017 Queen's University at Kingston
        ...
                ...
        
2) Run TXL program

    a) Test TXL program on Mytar ( In this case there is no problem):

        From folder: Mytar
            RUN: ./run.sh
        
    - Please check Mytar/errorFile.out to see any error message has been reported
    - The output files must have been created, see them in the folder: Mytar/output_files
    - An example of expected output is available in the folder: Mytar/output_expected.
    
    b) Test TXL program on MacVIM ( In this case the TXL Error Messages is showed):
    
        From folder: MacVIM
            RUN: ./run.sh
        
    - Please, check  MacVIM/errorFile.out to see the error message
    - Although the files are created, see them in the folder: MacVIM/output_files they are empth
    - Although the files are created, they are empth, see them in the folder: MacVIM/output_files
    
# REPOSITORY STRUCTURE

    transplantation_spl
    |_ MacVIM
        |_ input_files
        |_ macvim
        |_ output_files
    |_ Mytar 
        |_ Donor
        |_ input_files
        |_ output_expected
        |_ output_files
    |_ TXL
        
